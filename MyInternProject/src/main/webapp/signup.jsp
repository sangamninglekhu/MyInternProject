<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<link href="css/mycss.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="studentSignup">

<script>	
			function check_pass() {
			    if (document.getElementById('password').value ==
			            document.getElementById('confirmpassword').value) {
			        document.getElementById('submit').disabled = false;
			        document.getElementById('message').style.color = 'green';
			        document.getElementById('message').innerHTML = '';
			    } else {
			        document.getElementById('submit').disabled = true;
			        document.getElementById('message').style.color = 'red';
			        document.getElementById('message').innerHTML = 'password not matching';

			        
			    }
			}
</script>
		
	<form id="form" action="welcome" method="POST">

	
		
		<div style="color: #FF0000;">${errorMessage}</div>
		<div id="signup1">
		
		First Name: <br> 
			<input type="text" name="fname" required="required" /> <br> 
		Last Name: <br> 
			<input type="text" name="lname" required="required" /> <br> 
		Username: <br> 
			<input type="text" name="uname" required="required" /> <br> 
		Email: <br>
			<input type="email" name="email" /> <br> 
			
		</div>
			
		<div id="signup2">
			
		Age: <br> 
			<input type="number" name="age" required="required" /> <br> 
		Phone: <br>
			<input type="tel" name="phone" pattern="[0-9]{10}"
			title="Enter a 10 digit number" autofocus required /> <br> 
		Password: <br>
			<input type="password" id="password" name="password" onkeyup='check_pass();' required/> <br>
		Confirm Password: <br>
			<input type="password" id="confirmpassword" name="confirmpassword" onkeyup='check_pass();' required/><br> 
			<span id='message'></span>
			
		</div>
		
			<input type="submit" value="Submit"  id="submit" disabled/>
		
		
	</form>
</div>

</body>
</html>