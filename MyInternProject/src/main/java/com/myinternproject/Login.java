package com.myinternproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Login", urlPatterns = { "/welcomee" }, loadOnStartup = 1)

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uname = request.getParameter("username");
		String password = request.getParameter("password");
		Statement stm = null;
		String a, b;

		try {

			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/secondtest?verifyServerCertificate=false&useSSL=true", "root",
					"arthuria1850");
			stm = con.createStatement();
			ResultSet rs = stm.executeQuery("SELECT uname,password,fname FROM users");
			while (rs.next()) {
				a = rs.getString("uname");
				b = rs.getString("password");
				if (a.equals(uname) && b.equals(password)) {
					System.out.println("success");
					String fname = rs.getString("fname");
					request.setAttribute("namef", fname);
					request.getRequestDispatcher("welcome.jsp").forward(request, response);
					return;
					
				}
			}

			request.setAttribute("errorMessage",
					"You may have entered your username or password wrong. Please try again.");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				stm.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

}
