package com.myinternproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Signup", urlPatterns = { "/welcome" }, loadOnStartup = 1)

public class Signup extends HttpServlet {
	private static final long serialVersionUID = 123456789L;
	private Integer ag;

	public Signup() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String uname = request.getParameter("uname");
		String password = request.getParameter("password");

		try {
			ag = Integer.valueOf(request.getParameter("age"));
		} catch (Exception e) {
			System.out.println(e);
		}

		if (ag >= 13) {
			Statement stmt = null;
			try {
				
				// here inter is database name, root is username and password
				Connection con = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/secondtest?verifyServerCertificate=false&useSSL=true", "root",
						"arthuria1850");
				stmt = con.createStatement();
				stmt.executeUpdate("INSERT INTO users(fname,lname,uname,password,age,phone) VALUES ( '" + fname
						+ "' , '" + lname + "' , '" + uname + "' , '" + password + "' , " + ag + " , " + phone + ")");
				con.close();

			} catch (SQLException e) {
				System.out.println(e);
			}finally {
				try {
					stmt.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}

			request.setAttribute("namef", fname);
			request.getRequestDispatcher("welcome.jsp").forward(request, response);
		} else {
			request.setAttribute("errorMessage", "Sorry! You are age restricted");
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

}